#
# Copyright 2016, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Parsing log files for nginx and uwsgi as needed for data processing.
"""

import re
import os
import io
import sys
import gzip
import json
from urllib.parse import unquote

try:
    import user_agents
except ImportError:
    sys.stderr.write("Unable to parse user agents (install module)\n")
    #pylint: disable=invalid-name
    user_agents = None

from collections import Counter, defaultdict
from datetime import datetime, time

from django.contrib.gis.geoip import GeoIP
from django.conf import settings

from .utils import LogOpen, FastlyNetworks
from .settings import get_setting

GEOIP = GeoIP()
def country(value):
    """Returns the country code based on ip address"""
    ret = GEOIP.country_code(value)
    return str(ret) if ret else None

def get_logs_rex(key):
    logs = get_setting('LOGS')
    if key not in logs:
        raise ValueError("Log parser '%s' not found in LOGBOOK_PARSERS" % key)
    return logs[key]

def parse_file(key, log, reset=False, on_break=None, count=None):
    """
      Parse a log file with the given parser (key) and return a dictionary of
      paths, by a dictionary of dates, with a dictionary of metrics that may
      be a list of all values parsed.
    """
    logs = get_logs_rex(key)

    if not os.path.isfile(log):
        raise IOError("No %s log to process: %s" % (key, log))

    last_dtm = None
    with LogOpen(log, logs['rex'], reset=reset) as log:
        for kwargs in log:
            data = run(kwargs, *logs.get('ignore', ()))
            dtm = str(data.get('datetime', ''))
            if not dtm:
                continue
            if data['browser'] and 'Bot' in data['browser']:
                data['path'] = 'bot' # Group all bot requests
            if last_dtm != dtm:
                if last_dtm and on_break:
                    on_break(log.size, log.tell, log.count, dtm)
                last_dtm = dtm
            yield data
            if count is not None and log.count >= count:
                break
        if last_dtm and on_break:
            on_break(log.size, log.tell, log.count, dtm)

def run(data, *junk):
    """Attempt to format various keys and remove the junk"""
    data['M'] = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'].index(data['M'])
    dtm = datetime(*[int(data.pop(k)) for k in ['Y', 'M', 'D', 'h', 'm', 's']])
    data['date'] = dtm.date()
    # Every ten minutes
    data['datetime'] = datetime(dtm.year, dtm.month, dtm.day, dtm.hour, 0, 0)

    (local, data['lang'], data['path'], _) = url(data['path'].lower())

    (local, _, data['refer'], query) = url(data.get('refer', '-'))
    if local:
        data['link'] = data.pop('refer')
    else:
        (data['refer'], data['search']) = get_search(data['refer'] + '?' + query)

    data.update(dict(get_agent(data.pop('agent', None))))
    data['size'] = int(data['size'] or 0)
    data['cached'] = filter_fastly_ips(data['ip'])
    if data['browser'] and 'Bot' in data['browser']:
        data['country'] = None
    else:
        data['country'] = country(data.pop('ip'))

    for key in junk:
        data.pop(key, None)
    return dict(filter_data(data))

def filter_fastly_ips(address):
    """Detect fastly ip-address access and record network"""
    for network in FastlyNetworks():
        if address in network:
            return str(network)
    return None

def filter_data(data):
    for key, value in data.items():
        if value == '-':
            value = None
        # Usually the refer can be long
        if isinstance(value, str) and len(value) > 255:
            value = value[:255]
        if isinstance(value, tuple):
            if len(value) != 2:
                raise ValueError("Family, name pair error: %s" % str(value))
            value = (value[0][:128], value[1][:255])
        yield key, value


def get_search(url):
    for rex in get_setting('SEARCHES'):
        res = rex.match(url)
        if res:
            return ("search://" + res.groupdict()['site'],
                    res.groupdict().get('q', '').lower())
    return (url.split('?', 1)[0], None)

# The user agent module is REALLY SLOW so we must
# cache our results here to drasticly improve speed
AGENT_CACHE = {}

def get_agent(agent):
    """Use the user agent string to get browser and os"""
    if agent:
        uid = hash(agent)
        if uid not in AGENT_CACHE:
            if not user_agents:
                sys.stderr.write("Agent not parsed: %d=%s\n" % (uid, agent))
                AGENT_CACHE[uid] = []
            else:
                AGENT_CACHE[uid] = list(agent_filter(parse_agent(agent)))
        return AGENT_CACHE[uid]
    return []


def parse_agent(agent_string):
    """Get the agent string parsed into its useful parts"""
    agent = user_agents.parse(agent_string)
    # Not sure why user_agents doesn't parse windows
    if agent.os.family.startswith('Windows') and ' ' in agent.os.family:
        yield ('os',) + tuple(agent.os.family.split(' ', 1))
    elif agent.os.family != 'Other':
        yield ('os', agent.os.family, agent.os)
    else:
        yield ('os', '', '')

    if agent.browser.family != 'Other':
        yield ('browser', agent.browser.family, agent.browser)
    else:
        yield ('browser', '', '')

    if agent.device.family != 'Other':
        yield ('device', agent.device.brand, agent.device.family)
    else:
        yield ('device', '', '')

def agent_filter(items):
    """Filter user agent items to be more useful"""
    filters = get_setting('FILTERS', {})
    for (kind, family, version) in items:
        allowed = get_setting('ALLOWED_{}'.format(kind.upper()), [])
        version = agent_version(version)
        variable = "%s__%s" % (family, version)
        for (rex, replace, cont) in filters.get(kind, []):
            then = rex.sub(replace, variable)
            if then and '__' not in then:
                raise KeyError("Filter %s not family__version pair." % str(rex))
            elif then == '' or (then != variable and not cont):
                variable = then
                break
            variable = then
        if family:
            family = family.strip()
        if variable and family:
            (family, version) = variable.split('__', 1)
            if not allowed or family.strip() in allowed:
                if len(version.strip()) > 16:
                    version = 'Unknown'
                yield (kind, (family.strip(), version.strip()))
                continue
        if variable and family:
            #print(f"Bad {kind}: {family} ({version})")
            pass
        yield (kind, None)

def agent_version(agent_item):
    """We want to remove the least significant version information"""
    def lim(val):
        """Limit size of version string"""
        if isinstance(val, int) and val >= 1000:
            return str(val)[0] + '0' * (len(str(val)) - 1)
        return str(val)
    if isinstance(agent_item, str):
        return agent_item
    if isinstance(agent_item.version, str):
        return agent_item.version
    return '.'.join([lim(i) for i in agent_item.version[:2]])

def url(path):
    """Seperate out language and standardise url"""
    qs = ''
    local = False
    path = unquote(path)

    if '?' in path:
        (path, qs) = path.split('?', 1)

    if '://' in path:
        # For refers, we want to shorten them down to local urls.
        server = path.split('://')[-1].split('/')[0]
        if server in settings.ALLOWED_HOSTS:
            path = '/' + path.split('/', 3)[-1]
            local = True

    if path.startswith('/'):
        path = path.lstrip('/')
        if '/' in path:
            (lang, rest) = path.split('/', 1)
            lang = {
                'zh-tw': 'zh-hant',
                'zh': 'zh-hant',
            }.get(lang, lang)
            if lang in get_setting('LANGUAGE_CODES'):
                return (True, lang, rest.strip('/'), qs)

    return (local, None, path.strip('/'), qs)

