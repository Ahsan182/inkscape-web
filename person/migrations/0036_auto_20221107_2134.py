# Generated by Django 2.2 on 2022-11-07 21:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0035_auto_20211220_1608'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='email',
            field=models.CharField(blank=True, max_length=256, null=True, verbose_name='Email Address'),
        ),
        migrations.AlterField(
            model_name='teamapproval',
            name='approves',
            field=models.BooleanField(choices=[(True, 'Approve user'), (False, 'Disapprove user')], default=True, verbose_name='Team Action'),
        ),
        migrations.AlterField(
            model_name='teamapproval',
            name='reason',
            field=models.CharField(blank=True, help_text='A brief note of why you approve or disapprove of this membership.', max_length=255, null=True),
        ),
    ]
