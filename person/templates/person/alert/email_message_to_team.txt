{% load i18n %}
{% blocktrans with recipient=alert.user %}Dear {{ recipient }},{% endblocktrans %}
 
{% blocktrans with user=instance.admin team=instance %}Admin '{{ user }}' has sent the following message to all members of the '{{ team }}' team.{% endblocktrans %}

----------------

{{ body }}

