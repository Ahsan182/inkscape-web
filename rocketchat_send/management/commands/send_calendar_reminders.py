#
# Copyright 2021 Martin Owens
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Send rocket chat calendar reminders when needed
"""

import os
import sys
import time
import json
from collections import defaultdict
from datetime import timedelta

from django.db.models import Q
from django.utils.timezone import now

from calendars.templatetags.cal import make_ago
from calendars.models import Event
from rocketchat_send.api import send_message, get_api, get_rooms, full_url

from ..base_command import BaseCommand

# Number of minutes a status update is valid for, older are ignored.
REMINDER_TIMEOUT = timedelta(minutes=5)
# Logging marks for sent messages
SENT_STICKS = '0¹²³⁴⁵⁶⁷⁸⁹^'

rt_query = Q(room_type__name__icontains='rocketchat') |\
           Q(room_type__name__icontains='rocket chat')

class Command(BaseCommand):
    error = staticmethod(lambda msg: Command.msg(msg, "!"))
    warn = staticmethod(lambda msg: Command.msg(msg, "*"))
    help = __doc__
    sent_messages = {}
    event_cache = []
    event_dt = None
    rooms = None
    api = None

    def function(self):
        self.api = get_api()
        if self.api is None:
            self.msg("RocketChat is not configured, not running this process.")
            return

        self.rooms = get_rooms(self.api)

        for pk, dts in self.get_reminders().items():
            sent = False
            _next = None
            try:
                event = Event.objects.get(pk=pk)
            except Event.DoesNotExist:
                continue

            for dt in dts:
                sent_file = self.sent_file_for(event, dt)

                if not os.path.isfile(sent_file) and now() > dt:
                    with open(sent_file, 'w') as fhl:
                        if now() < dt + REMINDER_TIMEOUT:
                            fhl.write(json.dumps(list(self.send_message(event)), indent=2))
                        else:
                            fhl.write(f"Timeout:{REMINDER_TIMEOUT}")
                    sent = True

                elif _next is None or _next[1] > dt - now():
                    _next = (dt, dt - now())

            next_file = self.next_file_for(event)
            if sent or not os.path.isfile(next_file):
                with open(next_file, 'w') as fhl:
                    if _next:
                        fhl.write(_next[0].isoformat() + " : " + make_ago(_next[1]))
                    else:
                        fhl.write("Nothing. All done.")

    def generate_reminders(self):
        for event in Event.objects.filter(reminders__isnull=False).exclude(reminders=''):
            for x, dt in event.get_reminders():
                yield (event.pk, x), (dt, event)

    def send_message(self, event):
        """This message is due for a send"""
        msg = self.prepare_message(event)
        for chat in event.team.chatrooms.filter(rt_query):
            if msg and chat.channel in self.rooms:
                yield send_message(self.rooms[chat.channel], msg, self.api)

    def prepare_message(self, event):
        oc = event.next_occurance()
        if not oc:
            return None
        url = full_url(event.get_absolute_url())
        _in = make_ago(oc.start - now())
        if event.remind_all:
            return f"@all {event.title}: {url} {_in}"
        return f"{event.title}: {url} {_in}"
